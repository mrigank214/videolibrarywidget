# Video Library Widget

![videolibrary](https://user-images.githubusercontent.com/52398513/91635031-ad897880-ea12-11ea-9616-180ff11fd583.jpeg)

## Android Youtube Player
android-youtube-player is a stable and customizable open source YouTube player for Android. It provides a simple View that can be easily integrated in every Activity/Fragment.


## Dependency Used
dependencies {
  implementation 'com.pierfrancescosoffritti.androidyoutubeplayer:core:10.0.5'
}